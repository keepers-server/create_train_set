# Semi-Supervised Classifier for Creating Train Set
Meeting Minutes Below

## I. Goals

### Create a Classifier that Removes Bias from Current Data

## II. Specification

### 1. Create word embedding representation of sentences

Word embeddings is a way to represent each word as a vector. This representation is used because two words can be measured in similarity based on euclidean distance between the words. One can even use math equations to determine what combinations of words can mean.

    ex: king - man = queen 


### 2. Convert word embeddings to sentence embeddings

Similar to word embeddings, sentece embeddings can be used to understand if two sentences have similar meanings by measuring the euclidean distance between two vectors.

### 3. Take sentence embeddings and run through classifier
Classifer TBD. Recommended a Forest Classifier because it deals with imbalanced data better than most other ones.

### 4. Manually go through data and mark properly classifed data

### 5. Repeat until satisified


# Meeting Minutes
- The data that we have is a good starting point
- The data we currently have is biased 
- Recommended to create a semi-supervised model that will remove the bias from the data set that can be used for training more advanced models in the future
- Also recommended to create a word2vev model to work with the dictionary to make it more robust to similar/combinations of words
- Future work includeds:
    - Classifier based on age (not range of ages)
    - Classifier based on time sent or received
- It was also recommended to label the data in-house because the data is too specialized to realiably outsource

